package test;

public class MyServerUnite {

    @Test
    public void simpleTest() {
        int expected = 1;

        int actual = 1;

        assertEquals(expected, actual, "The actual value should  be equal to the expected one!");
    }
}
