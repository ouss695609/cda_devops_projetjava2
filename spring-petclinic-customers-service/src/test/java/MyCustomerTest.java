
// Importation des classes nécessaires
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.samples.petclinic.customers.CustomersServiceApplication;

@SpringBootTest(classes = CustomersServiceApplication.class)

@AutoConfigureMockMvc

public class MyCustomerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testFind() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/owners/1")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
